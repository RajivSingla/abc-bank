package com.abc.domain.account;

public abstract class AccountFactory {

	public static Account create(AccountType accountType) {

		if (accountType == AccountType.CHECKING) {
			return new CheckingAccount();

		} else if (accountType == AccountType.SAVINGS) {
			return new SavingsAccount();

		} else {
			return new MaxiSavingsAccount();
		}
	}
}
