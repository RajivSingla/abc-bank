package com.abc.domain.customer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.abc.domain.account.Account;
import com.abc.domain.account.AccountFactory;
import com.abc.domain.account.AccountType;
import com.abc.domain.interest.InterestVisitor;
import com.abc.domain.report.CustomerReport;
import com.abc.domain.report.Report;

public class DefaultCustomer implements Customer {

	private final String name;
	private final List<Account> accounts;

	public DefaultCustomer(String name) {
		this.name = name;
		this.accounts = new ArrayList<Account>();
	}

	public String getName() {
		return this.name;
	}

	public List<Account> getAccounts() {
		return Collections.unmodifiableList(this.accounts);
	}

	public Customer openAccount(AccountType accountType) {
		accounts.add(AccountFactory.create(accountType));
		return this;
	}

	public BigDecimal getCustomerInterest(final InterestVisitor interestVisitor) {

		BigDecimal customerInterest = BigDecimal.ZERO;

		for (Account account : accounts) {
			customerInterest = customerInterest.add(account.getAccountInterest(interestVisitor));
		}

		return customerInterest;
	}

	public void transfer(final Account fromAccount, final Account toAccount,
			final BigDecimal transferAmount) {

		if (fromAccount.getBalance().compareTo(transferAmount) >= 0) {
			fromAccount.withdraw(transferAmount);
			toAccount.deposit(transferAmount);
		} else {
			throw new IllegalArgumentException("Invalid Transfer Amount");
		}

	}

	public Report<Customer> getCustomerReport() {
		return new CustomerReport();
	}

}
