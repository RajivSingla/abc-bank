package com.abc.domain.report;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.Account;
import com.abc.domain.account.SavingsAccount;

public class AccountReportTest {

	private Account account;
	private Report<Account> accountReport;

	@Before
	public void before() {
		account = new SavingsAccount();
		account.deposit(new BigDecimal(1000));
		account.withdraw(new BigDecimal(100));

		accountReport = new AccountReport();
	}

	@Test
	public void testGetReportText() {

		String reportText = accountReport.getReportText(account);

		System.out.println(reportText);
	}

}
