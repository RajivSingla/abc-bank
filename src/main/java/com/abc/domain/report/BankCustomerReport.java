package com.abc.domain.report;

import com.abc.domain.bank.Bank;
import com.abc.domain.customer.Customer;

public class BankCustomerReport implements Report<Bank> {

	public String getReportText(Bank bank) {

		StringBuilder sb = new StringBuilder(1024);

		sb.append("Customer Summary");

		for (Customer customer : bank.getCustomers()) {
			int numberOfAccount = customer.getAccounts().size();
			sb.append("\n - " + customer.getName() + " (");
			sb.append(numberOfAccount);
			sb.append(numberOfAccount == 1 ? " account " : " accounts");
			sb.append(")");
		}

		return sb.toString();
	}

}
