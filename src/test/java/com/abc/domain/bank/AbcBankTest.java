package com.abc.domain.bank;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.AccountType;
import com.abc.domain.customer.Customer;
import com.abc.domain.customer.DefaultCustomer;
import com.abc.domain.interest.DefaultInterestVisitor;
import com.abc.domain.interest.InterestVisitor;
import com.abc.testutils.BankAssertions;

public class AbcBankTest {

	private static final String CUSTOMER_NAME_1 = "CUSTOMER1";
	private static final String CUSTOMER_NAME_2 = "CUSTOMER2";
	private Customer customer1;
	private Customer customer2;
	private Bank abcBank;
	private InterestVisitor interestVisitor;

	@Before
	public void before() {
		customer1 = new DefaultCustomer(CUSTOMER_NAME_1);
		customer1.openAccount(AccountType.CHECKING);
		customer1.openAccount(AccountType.SAVINGS);
		customer1.openAccount(AccountType.MAXI_SAVINGS);
		customer1.getAccounts().get(0).deposit(new BigDecimal(3000));
		customer1.getAccounts().get(1).deposit(new BigDecimal(3000));
		customer1.getAccounts().get(2).deposit(new BigDecimal(3000));

		customer2 = new DefaultCustomer(CUSTOMER_NAME_2);
		customer2.openAccount(AccountType.CHECKING);
		customer2.openAccount(AccountType.SAVINGS);
		customer2.openAccount(AccountType.MAXI_SAVINGS);
		customer2.getAccounts().get(0).deposit(new BigDecimal(3000));
		customer2.getAccounts().get(1).deposit(new BigDecimal(3000));
		customer2.getAccounts().get(2).deposit(new BigDecimal(3000));

		abcBank = new AbcBank();
		abcBank.addCustomer(customer1);
		abcBank.addCustomer(customer2);

		interestVisitor = new DefaultInterestVisitor();

	}

	@Test
	public void testAddCustomer() {
		assertTrue(abcBank.getCustomers().size() == 2);
	}

	@Test
	public void testGetCustomers() {
		assertTrue(abcBank.getCustomers().size() == 2);
		assertTrue(abcBank.getCustomers().get(0).getName().equals(CUSTOMER_NAME_1));
		assertTrue(abcBank.getCustomers().get(1).getName().equals(CUSTOMER_NAME_2));
	}

	@Test
	public void testGetBankInterest() {
		// Total expected Bank interest = 356
		BankAssertions.assertExpectedBankInterest(abcBank, interestVisitor, 356d);
	}

	@Test
	public void testGetFirstCustomer() {
		assertTrue(abcBank.getFirstCustomer().get().getName().equals(CUSTOMER_NAME_1));
	}

	@Test
	public void testGetBankCustomerReport() {
		assertFalse(abcBank.getBankCustomerReport().getReportText(abcBank).isEmpty());
	}

	@Test
	public void testGetBankInterestReport() {
		assertFalse(abcBank.getBankInterestReport(interestVisitor).getReportText(abcBank).isEmpty());

	}

}
