package com.abc.domain.transaction;

public enum TransactionType {
	CREDIT, DEBIT;
}
