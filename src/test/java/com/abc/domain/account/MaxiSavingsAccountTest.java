package com.abc.domain.account;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.interest.DefaultInterestVisitor;
import com.abc.domain.interest.InterestVisitor;
import com.abc.testutils.BankAssertions;

public class MaxiSavingsAccountTest {

	private InterestVisitor interestVisitor;
	private Account maxisavingsAccount;

	@Before
	public void before() {
		interestVisitor = new DefaultInterestVisitor();
		maxisavingsAccount = new MaxiSavingsAccount();
		maxisavingsAccount.deposit(new BigDecimal(3000));
	}

	@Test
	public void testGetAccountType() {
		assertTrue(maxisavingsAccount.getAccountType().equals(AccountType.MAXI_SAVINGS));
	}

	@Test
	public void testGetAccountInterest() {

		// 1000*.02 + 1000*.05 + 1000*.10 = 170
		BankAssertions.assertExpectedAccountInterest(maxisavingsAccount, interestVisitor, 170d);

		// withdraw 1000
		maxisavingsAccount.withdraw(new BigDecimal(1000));

		// 1000*.02 + 1000*.05 = 70
		BankAssertions.assertExpectedAccountInterest(maxisavingsAccount, interestVisitor, 70d);

		// withdraw 1000
		maxisavingsAccount.withdraw(new BigDecimal(1000));

		// 1000*.02 = 20
		BankAssertions.assertExpectedAccountInterest(maxisavingsAccount, interestVisitor, 20d);

	}

}
