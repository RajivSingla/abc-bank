package com.abc.domain.transaction;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class DefaultTransaction implements Transaction {

	private final BigDecimal amount;
	private final DateTime transactionDate;
	private final TransactionType transactionType;

	public DefaultTransaction(BigDecimal amount, TransactionType transactionType,
			DateTime transactionDate) {
		this.amount = amount;
		this.transactionDate = transactionDate;
		this.transactionType = transactionType;
	}

	public DefaultTransaction(BigDecimal amount, TransactionType transactionType) {
		this(amount, transactionType, DateTime.now());
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public DateTime getDate() {
		return this.transactionDate;
	}

	public TransactionType getTransactionType() {
		return this.transactionType;
	}

}
