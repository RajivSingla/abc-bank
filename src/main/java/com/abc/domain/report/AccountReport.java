package com.abc.domain.report;

import com.abc.domain.account.Account;
import com.abc.domain.transaction.Transaction;
import com.abc.domain.transaction.TransactionType;
import com.abc.util.BDUtils;

public class AccountReport implements Report<Account> {

	public String getReportText(Account account) {

		StringBuilder sb = new StringBuilder(1024);

		switch (account.getAccountType()) {

		case CHECKING:
			sb.append("Checking Account\n");
			break;
		case SAVINGS:
			sb.append("Savings Account\n");
			break;
		case MAXI_SAVINGS:
			sb.append("Maxi Savings Account\n");
			break;

		}

		for (Transaction transaction : account.getTransactions()) {
			sb.append(" ");
			sb.append(transaction.getTransactionType() == TransactionType.DEBIT ? "withdrawal "
					: "deposit    ");
			sb.append(BDUtils.toDollars(transaction.getAmount()) + "\n");
		}

		sb.append("Total " + BDUtils.toDollars(account.getBalance()));

		return sb.toString();
	}

}
