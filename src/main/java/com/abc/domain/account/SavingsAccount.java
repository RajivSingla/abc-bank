package com.abc.domain.account;

import java.math.BigDecimal;

import com.abc.domain.interest.InterestVisitor;

public class SavingsAccount extends AccountBase {

	@Override
	public AccountType getAccountType() {
		return AccountType.SAVINGS;
	}

	@Override
	public BigDecimal getAccountInterest(InterestVisitor interestVisitor) {
		return interestVisitor.visit(this);
	}

}
