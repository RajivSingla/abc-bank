package com.abc.domain.interest;

import static com.abc.util.BankConstants.CHECKING_INTEREST_TIER_1;
import static com.abc.util.BankConstants.MAXI_SAVINGS_INTEREST_RATE_TIER_1;
import static com.abc.util.BankConstants.MAXI_SAVINGS_INTEREST_RATE_TIER_2;
import static com.abc.util.BankConstants.MAXI_SAVINGS_INTEREST_RATE_TIER_3;
import static com.abc.util.BankConstants.SAVINGS_INTEREST_RATE_TIER_1;
import static com.abc.util.BankConstants.SAVINGS_INTEREST_RATE_TIER_2;

import java.math.BigDecimal;

import com.abc.domain.account.CheckingAccount;
import com.abc.domain.account.MaxiSavingsAccount;
import com.abc.domain.account.SavingsAccount;
import com.abc.util.BDUtils;

public class DefaultInterestVisitor implements InterestVisitor {

	// Checking accounts have a flat rate of 0.1%
	public BigDecimal visit(CheckingAccount checkingAccount) {

		return getSimpleInterest(checkingAccount.getBalance(), CHECKING_INTEREST_TIER_1);
	}

	// Savings accounts have a rate of 0.1% for the first $1000 then 0.2%
	public BigDecimal visit(SavingsAccount savingsAccount) {

		BigDecimal accountBalance = savingsAccount.getBalance();

		if (BDUtils.isGreaterThan(accountBalance, 1000)) {

			return getSimpleInterest(BDUtils.subtract(accountBalance, 1000),
					SAVINGS_INTEREST_RATE_TIER_2).add(
					getSimpleInterest(1000d, SAVINGS_INTEREST_RATE_TIER_1));

		} else {

			return getSimpleInterest(accountBalance, SAVINGS_INTEREST_RATE_TIER_1);
		}
	}

	// Maxi-Savings account have a rate of 2% for the first $1000 then 5% for
	// the next $1000 then 10%
	public BigDecimal visit(MaxiSavingsAccount maxiSavingsAccount) {

		BigDecimal accountBalance = maxiSavingsAccount.getBalance();

		if (BDUtils.isGreaterThan(accountBalance, 2000)) {

			return getSimpleInterest(BDUtils.subtract(accountBalance, 2000),
					MAXI_SAVINGS_INTEREST_RATE_TIER_3).add(
					getSimpleInterest(1000d, MAXI_SAVINGS_INTEREST_RATE_TIER_2)).add(
					getSimpleInterest(1000d, MAXI_SAVINGS_INTEREST_RATE_TIER_1));

		} else if (BDUtils.isGreaterThan(accountBalance, 1000)) {

			return getSimpleInterest(BDUtils.subtract(accountBalance, 1000),
					MAXI_SAVINGS_INTEREST_RATE_TIER_2).add(
					getSimpleInterest(1000d, MAXI_SAVINGS_INTEREST_RATE_TIER_1));

		} else {

			return getSimpleInterest(accountBalance, MAXI_SAVINGS_INTEREST_RATE_TIER_1);

		}

	}

	private static BigDecimal getSimpleInterest(BigDecimal amount, double interestRate) {
		return BDUtils.multiply(amount, interestRate);
	}

	private static BigDecimal getSimpleInterest(Double amount, double interestRate) {
		return getSimpleInterest(new BigDecimal(amount), interestRate);
	}

}
