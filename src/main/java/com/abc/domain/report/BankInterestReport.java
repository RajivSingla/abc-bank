package com.abc.domain.report;

import java.math.BigDecimal;

import com.abc.domain.account.Account;
import com.abc.domain.bank.Bank;
import com.abc.domain.customer.Customer;
import com.abc.domain.interest.InterestVisitor;
import com.abc.util.BDUtils;

public class BankInterestReport implements Report<Bank> {

	private final InterestVisitor interestVisitor;

	public BankInterestReport(InterestVisitor interestVisitor) {
		this.interestVisitor = interestVisitor;
	}

	public String getReportText(Bank bank) {

		StringBuilder sb = new StringBuilder(1024);

		sb.append("Interest Summary\n");

		BigDecimal bankInterest = BigDecimal.ZERO;

		for (Customer customer : bank.getCustomers()) {
			int numberOfAccount = customer.getAccounts().size();
			sb.append("\n - " + customer.getName() + " (");
			sb.append(numberOfAccount);
			sb.append(numberOfAccount == 1 ? " account " : " accounts");
			sb.append(")\n");

			for (Account account : customer.getAccounts()) {
				sb.append("   " + account.getAccountType() + "\t");
				sb.append(BDUtils.toDollars(account.getAccountInterest(interestVisitor)) + "\n");
			}

			sb.append("   Total Customer Interest "
					+ BDUtils.toDollars(customer.getCustomerInterest(interestVisitor)) + "\n");
			bankInterest = bankInterest.add(customer.getCustomerInterest(interestVisitor));
		}

		sb.append("\n-------------------------------\n");
		sb.append("Total Interest Paid by Bank " + BDUtils.toDollars(bankInterest));

		return sb.toString();
	}
}
