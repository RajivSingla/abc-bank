package com.abc.domain.account;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.abc.domain.interest.InterestVisitor;
import com.abc.domain.report.AccountReport;
import com.abc.domain.report.Report;
import com.abc.domain.transaction.DefaultTransaction;
import com.abc.domain.transaction.Transaction;
import com.abc.domain.transaction.TransactionType;
import com.abc.util.BDUtils;

public abstract class AccountBase implements Account {

	protected List<Transaction> transactions;
	protected BigDecimal accountBalance;

	public AccountBase() {
		this.transactions = new ArrayList<Transaction>();
		this.accountBalance = BigDecimal.ZERO;
	}

	public abstract AccountType getAccountType();

	public abstract BigDecimal getAccountInterest(InterestVisitor interestVisitor);

	public BigDecimal getBalance() {
		return this.accountBalance;
	}

	public List<Transaction> getTransactions() {
		return Collections.unmodifiableList(this.transactions);
	}

	public Account withdraw(BigDecimal amount) {
		validateAddTransaction(amount, TransactionType.DEBIT);
		return this;
	}

	public Account deposit(BigDecimal amount) {
		validateAddTransaction(amount, TransactionType.CREDIT);
		return this;
	}

	public Report<Account> getAccountReport() {
		return new AccountReport();
	}

	private void validateAddTransaction(BigDecimal amount, TransactionType transactionType) {

		if (BDUtils.isGreaterThanZero(amount)) {
			Transaction transaction = new DefaultTransaction(amount, transactionType);
			transactions.add(transaction);
			if (transactionType == TransactionType.CREDIT) {
				accountBalance = accountBalance.add(amount);
			} else {
				accountBalance = accountBalance.subtract(amount);
			}
		} else {
			throw new IllegalArgumentException("Transaction Amount must be greater than zero");
		}

	}
}
