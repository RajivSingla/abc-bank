package com.abc.domain.bank;

import java.math.BigDecimal;
import java.util.List;

import com.abc.domain.customer.Customer;
import com.abc.domain.interest.InterestVisitor;
import com.abc.domain.report.Report;
import com.google.common.base.Optional;

public interface Bank {

	public Bank addCustomer(Customer customer);

	public List<Customer> getCustomers();

	public BigDecimal getBankInterest(InterestVisitor interestVisitor);

	public Optional<Customer> getFirstCustomer();

	public Report<Bank> getBankCustomerReport();

	public Report<Bank> getBankInterestReport(InterestVisitor interestVisitor);

}
