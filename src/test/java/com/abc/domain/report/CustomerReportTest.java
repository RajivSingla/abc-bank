package com.abc.domain.report;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.AccountType;
import com.abc.domain.customer.Customer;
import com.abc.domain.customer.DefaultCustomer;

public class CustomerReportTest {

	private static final String CUSTOMER_NAME_1 = "CUSTOMER1";
	private Customer customer;
	private Report<Customer> customerReport;

	@Before
	public void before() {
		customer = new DefaultCustomer(CUSTOMER_NAME_1);
		customer.openAccount(AccountType.CHECKING);
		customer.openAccount(AccountType.SAVINGS);
		customer.openAccount(AccountType.MAXI_SAVINGS);

		customer.getAccounts().get(0).deposit(new BigDecimal(3000));
		customer.getAccounts().get(1).deposit(new BigDecimal(3000));
		customer.getAccounts().get(2).deposit(new BigDecimal(3000));

		customerReport = new CustomerReport();
	}

	@Test
	public void testGetReportText() {

		String reportText = customerReport.getReportText(customer);

		System.out.println(reportText);
	}

}
