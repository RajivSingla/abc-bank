package com.abc.domain.account;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.interest.DefaultInterestVisitor;
import com.abc.domain.interest.InterestVisitor;
import com.abc.testutils.BankAssertions;

public class CheckingAccountTest {

	private InterestVisitor interestVisitor;
	private Account checkingAccount;

	@Before
	public void before() {
		interestVisitor = new DefaultInterestVisitor();
		checkingAccount = new CheckingAccount();
		checkingAccount.deposit(new BigDecimal(3000));
	}

	@Test
	public void testGetAccountType() {
		assertTrue(checkingAccount.getAccountType().equals(AccountType.CHECKING));
	}

	@Test
	public void testGetAccountInterest() {

		// 3000*.001 = 3
		BankAssertions.assertExpectedAccountInterest(checkingAccount, interestVisitor, 3.00);

		// withdraw 2100
		checkingAccount.withdraw(new BigDecimal(2100));

		// 900*.001 = .9
		BankAssertions.assertExpectedAccountInterest(checkingAccount, interestVisitor, .9);

	}

}
