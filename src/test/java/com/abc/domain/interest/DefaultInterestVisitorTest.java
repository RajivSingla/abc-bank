package com.abc.domain.interest;

import static com.abc.testutils.BankAssertions.assertExpectedInterest;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.CheckingAccount;
import com.abc.domain.account.MaxiSavingsAccount;
import com.abc.domain.account.SavingsAccount;
import com.abc.util.BDUtils;

public class DefaultInterestVisitorTest {

	private DefaultInterestVisitor defaultInterestVisitor;
	private CheckingAccount checkingAccount;
	private SavingsAccount savingsAccount;
	private MaxiSavingsAccount maxiSavingsAccount;

	@Before
	public void before() {
		defaultInterestVisitor = new DefaultInterestVisitor();
		checkingAccount = new CheckingAccount();
		savingsAccount = new SavingsAccount();
		maxiSavingsAccount = new MaxiSavingsAccount();

		checkingAccount.deposit(new BigDecimal(3000));
		savingsAccount.deposit(new BigDecimal(3000));
		maxiSavingsAccount.deposit(new BigDecimal(3000));
	}

	@Test
	public void testCheckingAccountInterest() {
		// 3 for simple interest
		BigDecimal earnedInterest = defaultInterestVisitor.visit(checkingAccount);
		assertExpectedInterest(earnedInterest, 3);
	}

	@Test
	public void testSavingsAccountInterest() {
		// 5 for simple interest
		BigDecimal earnedInterest = defaultInterestVisitor.visit(savingsAccount);
		assertExpectedInterest(earnedInterest, 5);

	}

	@Test
	public void testMaxiSavingsAccountInterest() {
		// 170 for simple interest
		BigDecimal earnedInterest = defaultInterestVisitor.visit(maxiSavingsAccount);
		System.out.println(BDUtils.toDollars(earnedInterest));
		assertExpectedInterest(earnedInterest, 170);
	}

}
