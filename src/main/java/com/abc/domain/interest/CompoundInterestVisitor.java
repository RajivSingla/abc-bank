package com.abc.domain.interest;

import static com.abc.util.BankConstants.CHECKING_INTEREST_TIER_1;
import static com.abc.util.BankConstants.SAVINGS_INTEREST_RATE_TIER_1;
import static com.abc.util.BankConstants.SAVINGS_INTEREST_RATE_TIER_2;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.joda.time.Period;

import com.abc.domain.account.Account;
import com.abc.domain.account.CheckingAccount;
import com.abc.domain.account.MaxiSavingsAccount;
import com.abc.domain.account.SavingsAccount;
import com.abc.domain.transaction.Transaction;
import com.abc.domain.transaction.TransactionType;
import com.abc.util.BDUtils;

public class CompoundInterestVisitor implements InterestVisitor {

	// this is early compounded interest - not dependent on current time

	public BigDecimal visit(CheckingAccount checkingAccount) {

		return getCompundedInterest(checkingAccount.getBalance(), CHECKING_INTEREST_TIER_1, 365);
	}

	public BigDecimal visit(SavingsAccount savingsAccount) {

		BigDecimal accountBalance = savingsAccount.getBalance();

		if (BDUtils.isGreaterThan(accountBalance, 1000)) {

			return getCompundedInterest(BDUtils.subtract(accountBalance, 1000),
					SAVINGS_INTEREST_RATE_TIER_2, 365).add(
					getCompundedInterest(new BigDecimal(1000), SAVINGS_INTEREST_RATE_TIER_1, 365));

		} else {

			return getCompundedInterest(accountBalance, SAVINGS_INTEREST_RATE_TIER_1, 365);
		}

	}

	public BigDecimal visit(MaxiSavingsAccount maxiSavingsAccount) {
		// Maxi-Savings accounts have an interest rate of 5% assuming no
		// withdrawals in the past 10 days otherwise 0.1%
		BigDecimal accountBalance = maxiSavingsAccount.getBalance();

		if (isWithdrawalInLastDays(maxiSavingsAccount, 10)) {
			return getCompundedInterest(accountBalance, .001, 365);
		} else {
			return getCompundedInterest(accountBalance, .05, 365);
		}
	}

	// yearly compounded interest = principal * ( (1+rate/365)^days -1)
	private static BigDecimal getCompundedInterest(BigDecimal balance, double rate, int days) {
		return BDUtils.multiply(balance, (Math.pow((1 + (rate / 365)), days)) - 1);
	}

	// method to calculate current interest
	private static BigDecimal getCompundedInterest(Account account, double rate) {
		int days = getTransactionPeriod(account).getDays();
		return getCompundedInterest(account.getBalance(), rate, days);
	}

	private static Period getTransactionPeriod(Account account) {
		return new Period(account.getTransactions().get(0).getDate(), DateTime.now());
	}

	private static boolean isWithdrawalInLastDays(Account account, int days) {

		DateTime lastWithdrawal = null;
		for (Transaction transaction : account.getTransactions()) {
			if (transaction.getTransactionType() == TransactionType.DEBIT) {
				if (lastWithdrawal == null) {
					lastWithdrawal = transaction.getDate();
				}
				if (transaction.getDate().isAfter(lastWithdrawal)) {
					lastWithdrawal = transaction.getDate();
				}
			}
		}

		if (lastWithdrawal != null && lastWithdrawal.compareTo(DateTime.now().minusDays(days)) > 0) {
			return true;
		} else {
			return false;
		}
	}

}
