package com.abc.domain.report;

public interface Report<T> {

	public String getReportText(T entity);

}
