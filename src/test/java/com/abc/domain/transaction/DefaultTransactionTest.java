package com.abc.domain.transaction;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

public class DefaultTransactionTest {

	private DateTime transactionDate;
	private BigDecimal amount;
	private Transaction transaction;
	private TransactionType transactionType;

	@Before
	public void before() {
		transactionDate = DateTime.now();
		amount = BigDecimal.ONE;
		transactionType = TransactionType.CREDIT;
		transaction = new DefaultTransaction(amount, transactionType, transactionDate);
	}

	@Test
	public void testGetTransactionAmount() {
		assertTrue(transaction.getAmount().equals(amount));
	}

	@Test
	public void testGetTransactionDate() {
		assertTrue(transaction.getDate().equals(transactionDate));
	}

	@Test
	public void testGetTransactionType() {
		assertTrue(transaction.getTransactionType() == transactionType);
	}

}
