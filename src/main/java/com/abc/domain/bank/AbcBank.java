package com.abc.domain.bank;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.abc.domain.customer.Customer;
import com.abc.domain.interest.InterestVisitor;
import com.abc.domain.report.BankCustomerReport;
import com.abc.domain.report.BankInterestReport;
import com.abc.domain.report.Report;
import com.google.common.base.Optional;

public class AbcBank implements Bank {

	private final List<Customer> customers;

	public AbcBank() {
		this.customers = new ArrayList<Customer>();
	}

	public Bank addCustomer(Customer customer) {
		customers.add(customer);
		return this;
	}

	public List<Customer> getCustomers() {
		return Collections.unmodifiableList(customers);
	}

	public BigDecimal getBankInterest(InterestVisitor interestVisitor) {

		BigDecimal bankInterest = BigDecimal.ZERO;
		for (Customer customer : customers) {
			bankInterest = bankInterest.add(customer.getCustomerInterest(interestVisitor));
		}

		return bankInterest;
	}

	public Optional<Customer> getFirstCustomer() {

		if (!customers.isEmpty()) {
			return Optional.of(customers.get(0));
		} else {
			return Optional.absent();
		}

	}

	public Report<Bank> getBankCustomerReport() {
		return new BankCustomerReport();
	}

	public Report<Bank> getBankInterestReport(InterestVisitor interestVisitor) {
		return new BankInterestReport(interestVisitor);
	}
}
