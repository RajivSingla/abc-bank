package com.abc.util;

import java.math.BigDecimal;

public abstract class BDUtils {

	public static boolean isGreaterThanZero(BigDecimal amount) {
		return amount.compareTo(BigDecimal.ZERO) > 0;
	}

	public static boolean isGreaterThan(BigDecimal amount, double targetAmount) {
		return amount.compareTo(new BigDecimal(targetAmount)) > 0;
	}

	public static boolean isLessThanOrEqualTo(BigDecimal amount, double targetAmount) {
		return amount.compareTo(new BigDecimal(targetAmount)) <= 0;
	}

	public static BigDecimal subtract(BigDecimal amount, double subtractionAmount) {
		return amount.subtract(new BigDecimal(subtractionAmount));
	}

	public static BigDecimal multiply(BigDecimal amount, double interestRate) {
		return amount.multiply(new BigDecimal(interestRate), BankConstants.mathContext());
	}

	public static BigDecimal multiply(double amount, double interestRate) {
		return multiply(new BigDecimal(amount), interestRate);
	}

	public static String toDollars(BigDecimal amount) {
		return amount.setScale(BankConstants.BANK_CALCULATION_PRECISION).toPlainString();
	}

}
