package com.abc.domain.customer;

import java.math.BigDecimal;
import java.util.List;

import com.abc.domain.account.Account;
import com.abc.domain.account.AccountType;
import com.abc.domain.interest.InterestVisitor;
import com.abc.domain.report.Report;

public interface Customer {

	public String getName();

	public List<Account> getAccounts();

	public Customer openAccount(final AccountType accountType);

	public BigDecimal getCustomerInterest(final InterestVisitor interestVisitor);

	public void transfer(final Account fromAccount, final Account toAccount,
			final BigDecimal transferAmount);

	public Report<Customer> getCustomerReport();

}
