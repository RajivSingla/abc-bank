package com.abc.domain.account;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.util.BankConstants;

public class AccountBaseTest {

	private Account account;

	@Before
	public void before() {
		account = new SavingsAccount();
		account.deposit(new BigDecimal(1000));
		account.withdraw(new BigDecimal(100));
	}

	@Test
	public void testGetBalance() {
		BigDecimal accountBalance = account.getBalance();
		BigDecimal expectedBalance = new BigDecimal(900.0, BankConstants.mathContext());
		assertTrue(accountBalance.compareTo(expectedBalance) == 0);
	}

	@Test
	public void testGetTransactions() {
		assertTrue(account.getTransactions().size() == 2);
	}

	@Test
	public void testWithdraw() {
		SavingsAccount testSavingsAccount = new SavingsAccount();
		BigDecimal withdrawalAmount = new BigDecimal(100);
		testSavingsAccount.withdraw(withdrawalAmount);
		assertTrue(testSavingsAccount.getTransactions().size() == 1);
		assertTrue(testSavingsAccount.getBalance().compareTo(withdrawalAmount.negate()) == 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidWithdrawal() {
		SavingsAccount testSavingsAccount = new SavingsAccount();
		BigDecimal invalidWithdrawalAmount = new BigDecimal(0);
		testSavingsAccount.withdraw(invalidWithdrawalAmount);
	}

	@Test
	public void testDeposit() {
		SavingsAccount testSavingsAccount = new SavingsAccount();
		BigDecimal depositAmount = new BigDecimal(100);
		testSavingsAccount.deposit(depositAmount);
		assertTrue(testSavingsAccount.getTransactions().size() == 1);
		assertTrue(testSavingsAccount.getBalance().compareTo(depositAmount) == 0);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDeposit() {
		SavingsAccount testSavingsAccount = new SavingsAccount();
		BigDecimal invalidDepositAmount = new BigDecimal(0);
		testSavingsAccount.deposit(invalidDepositAmount);
	}

	@Test
	public void testGetAccountReport() {
		assertFalse(account.getAccountReport().getReportText(account).isEmpty());
	}

}
