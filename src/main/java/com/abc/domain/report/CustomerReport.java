package com.abc.domain.report;

import java.math.BigDecimal;

import com.abc.domain.account.Account;
import com.abc.domain.customer.Customer;
import com.abc.util.BDUtils;

public class CustomerReport implements Report<Customer> {

	public String getReportText(Customer customer) {

		StringBuilder sb = new StringBuilder(1024);
		sb.append("Statement for " + customer.getName() + "\n");

		BigDecimal customerBalance = BigDecimal.ZERO;

		for (Account account : customer.getAccounts()) {
			sb.append(account.getAccountReport().getReportText(account) + "\n");
			customerBalance = customerBalance.add(account.getBalance());
		}

		sb.append("Total in all Account " + BDUtils.toDollars(customerBalance));

		return sb.toString();
	}

}
