package com.abc.domain.transaction;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public interface Transaction {

	public BigDecimal getAmount();

	public DateTime getDate();

	public TransactionType getTransactionType();

}
