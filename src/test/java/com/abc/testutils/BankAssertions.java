package com.abc.testutils;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import com.abc.domain.account.Account;
import com.abc.domain.bank.Bank;
import com.abc.domain.customer.Customer;
import com.abc.domain.interest.InterestVisitor;
import com.abc.util.BankConstants;

public abstract class BankAssertions {

	public static void assertExpectedAccountInterest(Account account,
			InterestVisitor interestVisitor, double expectedInterest) {

		BigDecimal earnedInterest = account.getAccountInterest(interestVisitor);
		BigDecimal expectedInt = new BigDecimal(expectedInterest, BankConstants.mathContext());
		assertTrue(earnedInterest.compareTo(expectedInt) == 0);
	}

	public static void assertExpectedCustomerInterest(Customer customer,
			InterestVisitor interestVisitor, double expectedInterest) {

		BigDecimal earnedInterest = customer.getCustomerInterest(interestVisitor);
		BigDecimal expectedInt = new BigDecimal(expectedInterest);
		assertTrue(earnedInterest.compareTo(expectedInt) == 0);
	}

	public static void assertExpectedBankInterest(Bank bank, InterestVisitor interestVisitor,
			double expectedInterest) {

		BigDecimal earnedInterest = bank.getBankInterest(interestVisitor);
		BigDecimal expectedInt = new BigDecimal(expectedInterest);
		assertTrue(earnedInterest.compareTo(expectedInt) == 0);
	}

	public static void assertExpectedInterest(BigDecimal earnedInterest, double expectedInterest) {
		BigDecimal expectedInt = new BigDecimal(expectedInterest);
		assertTrue(earnedInterest.compareTo(expectedInt) == 0);
	}

}
