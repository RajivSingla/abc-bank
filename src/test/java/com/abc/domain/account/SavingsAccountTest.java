package com.abc.domain.account;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.interest.DefaultInterestVisitor;
import com.abc.domain.interest.InterestVisitor;
import com.abc.testutils.BankAssertions;

public class SavingsAccountTest {

	private InterestVisitor interestVisitor;
	private Account savingsAccount;

	@Before
	public void before() {
		interestVisitor = new DefaultInterestVisitor();
		savingsAccount = new SavingsAccount();
		savingsAccount.deposit(new BigDecimal(3000));
	}

	@Test
	public void testGetAccountType() {
		assertTrue(savingsAccount.getAccountType().equals(AccountType.SAVINGS));
	}

	@Test
	public void testGetAccountInterest() {

		// 1000*.001 + 2000*.002 = 5
		BankAssertions.assertExpectedAccountInterest(savingsAccount, interestVisitor, 5d);

		// withdraw 2100
		savingsAccount.withdraw(new BigDecimal(2100));

		// 900*.001 = .9
		BankAssertions.assertExpectedAccountInterest(savingsAccount, interestVisitor, .9);

	}

}
