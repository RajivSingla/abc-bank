package com.abc.domain.customer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.AccountType;
import com.abc.domain.interest.DefaultInterestVisitor;
import com.abc.domain.interest.InterestVisitor;
import com.abc.testutils.BankAssertions;

public class DefaultCustomerTest {

	private static final String CUSTOMER_NAME_1 = "CUSTOMER1";
	private Customer customer;
	private InterestVisitor interestVisitor;

	@Before
	public void before() {
		customer = new DefaultCustomer(CUSTOMER_NAME_1);
		customer.openAccount(AccountType.CHECKING);
		customer.openAccount(AccountType.SAVINGS);
		customer.openAccount(AccountType.MAXI_SAVINGS);

		customer.getAccounts().get(0).deposit(new BigDecimal(3000));
		customer.getAccounts().get(1).deposit(new BigDecimal(3000));
		customer.getAccounts().get(2).deposit(new BigDecimal(3000));

		interestVisitor = new DefaultInterestVisitor();

	}

	@Test
	public void testGetName() {
		assertTrue(customer.getName().equals(CUSTOMER_NAME_1));
	}

	@Test
	public void testGetAccounts() {
		assertTrue(customer.getAccounts().size() == 3);
		assertTrue(customer.getAccounts().get(0).getAccountType() == AccountType.CHECKING);
		assertTrue(customer.getAccounts().get(1).getAccountType() == AccountType.SAVINGS);
		assertTrue(customer.getAccounts().get(2).getAccountType() == AccountType.MAXI_SAVINGS);

	}

	@Test
	public void testOpenAccount() {
		Customer testCustomer = new DefaultCustomer(CUSTOMER_NAME_1);
		testCustomer.openAccount(AccountType.CHECKING);
		assertTrue(testCustomer.getAccounts().size() == 1);
	}

	@Test
	public void testGetCustomerInterest() {
		// Total expectedInterest 178
		BankAssertions.assertExpectedCustomerInterest(customer, interestVisitor, 178d);
	}

	@Test
	public void testTransfer() {

		Customer testCustomer = new DefaultCustomer(CUSTOMER_NAME_1);
		testCustomer.openAccount(AccountType.CHECKING);
		testCustomer.openAccount(AccountType.SAVINGS);

		BigDecimal depositAmount = new BigDecimal(3000);
		testCustomer.getAccounts().get(0).deposit(depositAmount);
		testCustomer.getAccounts().get(1).deposit(depositAmount);

		assertTrue(testCustomer.getAccounts().get(0).getBalance().compareTo(depositAmount) == 0);
		assertTrue(testCustomer.getAccounts().get(1).getBalance().compareTo(depositAmount) == 0);

		BigDecimal transferAmount = new BigDecimal(3000);
		testCustomer.transfer(testCustomer.getAccounts().get(0), testCustomer.getAccounts().get(1),
				transferAmount);
		;

		assertTrue(testCustomer.getAccounts().get(0).getBalance().compareTo(BigDecimal.ZERO) == 0);
		assertTrue(testCustomer.getAccounts().get(1).getBalance().compareTo(new BigDecimal(6000)) == 0);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidTransfer() {

		Customer testCustomer = new DefaultCustomer(CUSTOMER_NAME_1);
		testCustomer.openAccount(AccountType.CHECKING);
		testCustomer.openAccount(AccountType.SAVINGS);

		BigDecimal depositAmount = new BigDecimal(3000);
		testCustomer.getAccounts().get(0).deposit(depositAmount);
		testCustomer.getAccounts().get(1).deposit(depositAmount);

		assertTrue(testCustomer.getAccounts().get(0).getBalance().compareTo(depositAmount) == 0);
		assertTrue(testCustomer.getAccounts().get(1).getBalance().compareTo(depositAmount) == 0);

		BigDecimal invalidTransferAmount = new BigDecimal(3000.01);
		testCustomer.transfer(testCustomer.getAccounts().get(0), testCustomer.getAccounts().get(1),
				invalidTransferAmount);
		;

	}

	@Test
	public void testGetCustomerReport() {
		assertFalse(customer.getCustomerReport().getReportText(customer).isEmpty());
	}

}
