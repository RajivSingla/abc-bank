package com.abc.domain.report;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.AccountType;
import com.abc.domain.bank.AbcBank;
import com.abc.domain.bank.Bank;
import com.abc.domain.customer.Customer;
import com.abc.domain.customer.DefaultCustomer;
import com.abc.domain.interest.DefaultInterestVisitor;
import com.abc.domain.interest.InterestVisitor;

public class BankInterestReportTest {

	private static final String CUSTOMER_NAME_1 = "CUSTOMER1";
	private static final String CUSTOMER_NAME_2 = "CUSTOMER2";
	private Customer customer1;
	private Customer customer2;
	private Bank abcBank;
	private Report<Bank> bankInterestReport;
	private InterestVisitor interestVisitor;

	@Before
	public void before() {
		customer1 = new DefaultCustomer(CUSTOMER_NAME_1);
		customer1.openAccount(AccountType.CHECKING);
		customer1.openAccount(AccountType.SAVINGS);
		customer1.openAccount(AccountType.MAXI_SAVINGS);
		customer1.getAccounts().get(0).deposit(new BigDecimal(3000));
		customer1.getAccounts().get(1).deposit(new BigDecimal(3000));
		customer1.getAccounts().get(2).deposit(new BigDecimal(3000));

		customer2 = new DefaultCustomer(CUSTOMER_NAME_2);
		customer2.openAccount(AccountType.CHECKING);
		customer2.getAccounts().get(0).deposit(new BigDecimal(3000));

		abcBank = new AbcBank();
		abcBank.addCustomer(customer1);
		abcBank.addCustomer(customer2);

		interestVisitor = new DefaultInterestVisitor();
		bankInterestReport = new BankInterestReport(interestVisitor);

	}

	@Test
	public void testGetReportText() {

		String reportText = bankInterestReport.getReportText(abcBank);

		System.out.println(reportText);
	}

}
