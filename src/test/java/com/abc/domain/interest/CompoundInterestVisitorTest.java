package com.abc.domain.interest;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.abc.domain.account.CheckingAccount;
import com.abc.domain.account.MaxiSavingsAccount;
import com.abc.domain.account.SavingsAccount;
import com.abc.util.BDUtils;

public class CompoundInterestVisitorTest {

	private CompoundInterestVisitor compoundedInterestVisitor;
	private CheckingAccount checkingAccount;
	private SavingsAccount savingsAccount;
	private MaxiSavingsAccount maxiSavingsAccount;

	@Before
	public void before() {
		compoundedInterestVisitor = new CompoundInterestVisitor();
		checkingAccount = new CheckingAccount();
		savingsAccount = new SavingsAccount();
		maxiSavingsAccount = new MaxiSavingsAccount();

		checkingAccount.deposit(new BigDecimal(3000));
		savingsAccount.deposit(new BigDecimal(3000));
		maxiSavingsAccount.deposit(new BigDecimal(3000)).withdraw(new BigDecimal(1000));
	}

	@Test
	public void testCheckingAccountInterest() {
		// 3 for simple interest
		System.out.println(BDUtils.toDollars(compoundedInterestVisitor.visit(checkingAccount)));
	}

	@Test
	public void testSavingsAccountInterest() {
		// 5 for simple interest
		System.out.println(BDUtils.toDollars(compoundedInterestVisitor.visit(savingsAccount)));

	}

	@Test
	public void testMaxiSavingsAccountInterest() {

		System.out.println(BDUtils.toDollars(compoundedInterestVisitor.visit(maxiSavingsAccount)));

	}

}
