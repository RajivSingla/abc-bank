package com.abc.util;

import java.math.MathContext;

public abstract class BankConstants {

	public static final int BANK_CALCULATION_PRECISION = 4;

	public static final MathContext mathContext() {
		return new MathContext(BANK_CALCULATION_PRECISION);
	}

	public static final double CHECKING_INTEREST_TIER_1 = 0.001;

	public static final double SAVINGS_INTEREST_RATE_TIER_1 = 0.001;
	public static final double SAVINGS_INTEREST_RATE_TIER_2 = 0.002;

	public static final double MAXI_SAVINGS_INTEREST_RATE_TIER_1 = 0.02;
	public static final double MAXI_SAVINGS_INTEREST_RATE_TIER_2 = 0.05;
	public static final double MAXI_SAVINGS_INTEREST_RATE_TIER_3 = 0.10;

}
