package com.abc.domain.interest;

import java.math.BigDecimal;

import com.abc.domain.account.CheckingAccount;
import com.abc.domain.account.MaxiSavingsAccount;
import com.abc.domain.account.SavingsAccount;

public interface InterestVisitor {

	public BigDecimal visit(CheckingAccount checkingAccount);

	public BigDecimal visit(SavingsAccount savingsAccount);

	public BigDecimal visit(MaxiSavingsAccount maxiSavingsAccount);

}
