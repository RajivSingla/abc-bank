package com.abc.domain.account;

public enum AccountType {

	CHECKING, SAVINGS, MAXI_SAVINGS;

}
