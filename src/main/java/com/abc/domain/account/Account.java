package com.abc.domain.account;

import java.math.BigDecimal;
import java.util.List;

import com.abc.domain.interest.InterestVisitor;
import com.abc.domain.report.Report;
import com.abc.domain.transaction.Transaction;

public interface Account {

	public AccountType getAccountType();

	public BigDecimal getBalance();

	public List<Transaction> getTransactions();

	public Account withdraw(BigDecimal amount);

	public Account deposit(BigDecimal amount);

	public BigDecimal getAccountInterest(InterestVisitor interestVisitor);

	public Report<Account> getAccountReport();

}
