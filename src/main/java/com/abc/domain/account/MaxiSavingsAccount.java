package com.abc.domain.account;

import java.math.BigDecimal;

import com.abc.domain.interest.InterestVisitor;

public class MaxiSavingsAccount extends AccountBase {

	@Override
	public AccountType getAccountType() {
		return AccountType.MAXI_SAVINGS;
	}

	@Override
	public BigDecimal getAccountInterest(InterestVisitor interestVisitor) {
		return interestVisitor.visit(this);
	}

}
